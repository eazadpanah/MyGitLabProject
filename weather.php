<!DOCTYPE html>
<head>
    <title> welcome to my php learning website </title>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div>
    <h2 class="header">Welcome to PHP Trainee Web Site </h2>
</div>
<b style="align-content: center; color: pink">search movies </b>
<div id="tabs">
    <ul>
        <?php
        include "menu";
        ?>
    </ul>
</div>
<form action="?" method="get">
    <input type="text" name="city" placeholder="enter city name : "><br><br>
    <input type="submit" name="search"><br><br>
</form>

<?php
$city = $_GET['city'];
if (!empty($city)) {
    $url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22$city%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    $result = file_get_contents($url);
    $weatherArrays = json_decode($result, true);
    $temp = $weatherArrays['query']['results']['channel']['item']['condition']['temp'];
    echo "temp of $city is $temp F";
    echo "<br>";
    $c = ($temp - 32) / 1.8;
    echo "temp of $city is $c C";
}
?>

<br><br>
<?php
include "footer";
?>
</body>
</html>
