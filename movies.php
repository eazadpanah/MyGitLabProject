<!DOCTYPE html>
<head>
    <title> welcome to my php learning website </title>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div>
    <h2 class="header">Welcome to PHP Trainee Web Site </h2>
</div>
<b style="align-content: center; color: pink">search movies </b>
<div id="tabs">
    <ul>
        <?php
        include "menu";
        ?>
    </ul>
</div>
<form action="?" method="get">
    <input type="text" name="word" placeholder="enter movie word"><br><br>
    <input type="submit" name="search"><br><br>
</form>

<?php
$word = $_GET['word'];
if (!empty($word)) {
    $url = "http://www.omdbapi.com/?t=$word&apikey=9660afe8";
    $result = file_get_contents($url);
   // echo $result;
    $movieArrays=json_decode($result, true);

    echo "Title: " . $movieArrays['Title']."<br>";
    echo "year: " .$movieArrays['Year']."<br>";
    echo "Country:" .$movieArrays['Country']."<br>";
    echo "Genre:".$movieArrays['Genre']."<br>";
    echo "Director:".$movieArrays['Director']."<br>";
    echo "Language:".$movieArrays['Language']."<br>";
    //echo "poster: ". $movieArrays['Poster']. "<br>";
    $poster=$movieArrays['Poster'];
    echo "<img src='$poster' width='250px'>";
}
?>

<br><br>
<?php
include "footer";
?>
</body>
</html>
