<!DOCTYPE html>
<head>
    <title> welcome to my php learning website </title>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>

<div>
    <h2 class="header">Arrays </h2>
</div>

<div id="tabs">
    <ul>
        <?php
        include "menu";
        ?>
    </ul>
</div>

<?php
echo "<br>------------------------------------one dimentional arrays------------------------------------<br>";

$cars = array("BMV", "Toyota", "Mazda", "MVM");
var_dump($cars);
echo "<br><br>";
echo $cars[2];
echo "<br><br>";
foreach ($cars as $value) {
    echo "<br>" . $value;
}

echo "<br>------------------------------------array's pointers-------------------------------------------<br>";
$mycar = current($cars);
echo $mycar . "<br>";
echo $mycar = next($cars);
echo "<br>------------------------------------two dimentional array--------------------------------------<br>";

$carsyear = array(
    "BMV" => "1902",
    "Toyota" => "1900",
    "Mazda" => "1915",
    "MVM" => "1915",
);
foreach ($carsyear as $key => $value) {
    echo $key . " " . " 's produce year is :   " . $value . "<br>";
}

echo "<br>-------------------------------------tree dimentional array--------------------------------------<br>";
$multiarray = array(
    "Ali" => 65,
    "Reza" => "Ahmadi",
    452 => 896,
    "BigO" => array("Eyes" => "Optic")
);
var_dump($multiarray);

echo "<br><br>------------------------------foreach for function which return array------------------------<br>";

function get_students()
{
    return array("Alireza", "Amir", "Sara", "Ali");
}

foreach (get_students() as $value) {
    echo "<br>" . $value;
}
?>
<br><br>
<?php
include "footer";
?>
</body>
</html>

